<?php
session_start();
if ($_SESSION) { ?>
<!DOCTYPE html>
<html lang="es">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="../css/bootstrap.css" type="text/css">
  <!-- Estilos -->
  <link rel="stylesheet" href="../css/estilos.css" type="text/css">
  <!-- fuentes -->
  <link rel="stylesheet" href="../css/font-awesome.min.css" type="text/css">
  <title>Usuarios</title>
</head>

<body>
  <section id="navbar">

    <div class="row-fluid">
      <nav class="text-center navbar navbar-inverse navbar-toggleable-md navbar-light bg-inverse">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar"
          aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="col-lg-3">
          <a class="navbar-brand" href="../Desarrollo/index.php">GPI - Fincas</a>
        </div>
        <div class="col-lg-7">&nbsp;</div>
        <div class="col-lg-2">
          <a href="../inc/salir.php" class="btn btn-outline-danger my-2 my-sm-0"><i class="fa fa-sign-out" aria-hidden="true"></i>Cerrar
            Sesion</a>
        </div>
      </nav>
    </div>
  </section>

  <section id="jumbotron">

    <div class="jumbotron bg-dark">
      <h2 class="text-center">Estas en el modulo de control de usuarios
        <?php include 'operaciones.php'; imprimirnombre();?>
      </h2>
      <p class="text-center">A continuacion escoja que desea realizar</p>
    </div>
  </section>

  <section>
    <div class="container">

      <div class="alert alert-danger text-center" role="alert">
        <strong>Recuerda!</strong> que cualquier usuario que sea creado va a obtener acceso a operaciones como
        informacion de elementos y demas de la empresa.
      </div>
    </div>
  </section>

  <section id="acciones">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <h3 class="card-header bg-dark">Usuarios</h3>
            <div class="card-block">
              <h4 class="card-title">Crear Usuario</h4>
              <p class="card-text">En este apartado podras crear los usuarios que podran ingresar al sistema</p>
              <a href="../usuarios/crea_usuarios.php" class="btn btn-outline-success my-2 my-sm-0">Ingresar</a>

            </div>
            <div class="card-block">
              <h4 class="card-title">Consultar Usuarios</h4>
              <p class="card-text">En este apartado podra consultar los usuarios ademas de realizar otras funciones de
                gestion de información </p>
              <a href="../usuarios/consulta_usuarios.php" class="btn btn-outline-success my-2 my-sm-0">Ingresar</a>
            </div>
          </div>
        </div>

      </div>
    </div>


  </section>
  <div class="col-lg-12">&nbsp;</div>
  <div class="col-lg-12">&nbsp;</div>
  <div class="col-lg-12">&nbsp;</div>

</body>

</html>
<?php
}
else {
	echo "<script type='text/javascript'>
		alert('Ud no ha iniciado sesion. Por favor iniciar una o registrese');
		window.location='/index.html';
	</script>";
} ?>