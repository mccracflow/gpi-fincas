-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-10-2018 a las 05:14:56
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gpi-fincas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fincas`
--

CREATE TABLE `fincas` (
  `idFincas` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Direccion` varchar(45) DEFAULT NULL,
  `Cuidad` varchar(45) DEFAULT NULL,
  `Telefono` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `fincas`
--

INSERT INTO `fincas` (`idFincas`, `Nombre`, `Direccion`, `Cuidad`, `Telefono`) VALUES
(1, 'Esmeralda', 'vereda esmeralda km 15 via calarca', 'risaralda', '5687595'),
(2, 'rubi', 'km 6 via la calera', 'cundinamarca', '2385963');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login`
--

CREATE TABLE `login` (
  `correo` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `usuario_idusuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `login`
--

INSERT INTO `login` (`correo`, `password`, `usuario_idusuario`) VALUES
('mario@gmail.com', '123456', 1014207968),
('fabian@gmail.com', '987654', 1014258963);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

CREATE TABLE `personal` (
  `idPersonal` int(11) NOT NULL,
  `usuario_idusuario` int(11) NOT NULL,
  `Fincas_idFincas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `personal`
--

INSERT INTO `personal` (`idPersonal`, `usuario_idusuario`, `Fincas_idFincas`) VALUES
(201, 1014207968, 1),
(202, 1014258963, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `idProductos` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Peso` varchar(45) DEFAULT NULL,
  `Fecha-de-adquisicion` varchar(45) DEFAULT NULL,
  `Fecha-de-venta` varchar(45) DEFAULT NULL,
  `Tipo_idTipo` int(11) NOT NULL,
  `Valor_idCosto` int(11) NOT NULL,
  `Personal_idPersonal` int(11) NOT NULL,
  `Fincas_idFincas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`idProductos`, `Nombre`, `Peso`, `Fecha-de-adquisicion`, `Fecha-de-venta`, `Tipo_idTipo`, `Valor_idCosto`, `Personal_idPersonal`, `Fincas_idFincas`) VALUES
(301, 'res', '500', '10/10/2017', 'actual', 401, 501, 201, 1),
(302, 'cerdo', '300', '15/02/2018', '11/10/2018', 401, 502, 202, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo`
--

CREATE TABLE `tipo` (
  `idTipo` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo`
--

INSERT INTO `tipo` (`idTipo`, `Nombre`) VALUES
(401, 'interno'),
(402, 'externo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `Documento` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Apellidos` varchar(45) DEFAULT NULL,
  `Correo` varchar(45) DEFAULT NULL,
  `Cargo` varchar(45) DEFAULT NULL,
  `Telefono` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`Documento`, `Nombre`, `Apellidos`, `Correo`, `Cargo`, `Telefono`) VALUES
(1014207968, 'mario', 'gonzalez', 'mgonzalezb4@uniminuto.edu.co', 'DBA', '7856431'),
(1014258963, 'hector ', 'rodriguez', 'hrodrigue26@uniminuto.edu.co', 'programador', '4567890');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valor`
--

CREATE TABLE `valor` (
  `idCosto` int(11) NOT NULL,
  `Compra` varchar(45) DEFAULT NULL,
  `Venta` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `valor`
--

INSERT INTO `valor` (`idCosto`, `Compra`, `Venta`) VALUES
(501, '1000000', '1100000'),
(502, '1520000', '1672000');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `fincas`
--
ALTER TABLE `fincas`
  ADD PRIMARY KEY (`idFincas`);

--
-- Indices de la tabla `login`
--
ALTER TABLE `login`
  ADD KEY `fk_table1_usuario_idx` (`usuario_idusuario`);

--
-- Indices de la tabla `personal`
--
ALTER TABLE `personal`
  ADD PRIMARY KEY (`idPersonal`),
  ADD KEY `fk_Personal_usuario1_idx` (`usuario_idusuario`),
  ADD KEY `fk_Personal_Fincas1_idx` (`Fincas_idFincas`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`idProductos`),
  ADD KEY `fk_Productos_Tipo1_idx` (`Tipo_idTipo`),
  ADD KEY `fk_Productos_Valor1_idx` (`Valor_idCosto`),
  ADD KEY `fk_Productos_Personal1_idx` (`Personal_idPersonal`),
  ADD KEY `fk_Productos_Fincas1_idx` (`Fincas_idFincas`);

--
-- Indices de la tabla `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`idTipo`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`Documento`);

--
-- Indices de la tabla `valor`
--
ALTER TABLE `valor`
  ADD PRIMARY KEY (`idCosto`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `login`
--
ALTER TABLE `login`
  ADD CONSTRAINT `fk_table1_usuario` FOREIGN KEY (`usuario_idusuario`) REFERENCES `usuario` (`Documento`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `personal`
--
ALTER TABLE `personal`
  ADD CONSTRAINT `fk_Personal_Fincas1` FOREIGN KEY (`Fincas_idFincas`) REFERENCES `fincas` (`idFincas`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Personal_usuario1` FOREIGN KEY (`usuario_idusuario`) REFERENCES `usuario` (`Documento`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `fk_Productos_Fincas1` FOREIGN KEY (`Fincas_idFincas`) REFERENCES `fincas` (`idFincas`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Productos_Personal1` FOREIGN KEY (`Personal_idPersonal`) REFERENCES `personal` (`idPersonal`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Productos_Tipo1` FOREIGN KEY (`Tipo_idTipo`) REFERENCES `tipo` (`idTipo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Productos_Valor1` FOREIGN KEY (`Valor_idCosto`) REFERENCES `valor` (`idCosto`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
